HealAll = {}

function HealAll.Initialize()
	RegisterEventHandler(SystemData.Events.INTERACT_SHOW_HEALER , "HealAll.Heal")

	EA_ChatWindow.Print(towstring("HealAll Loaded"))
end
function HealAll.Heal()
d(".Heal")
	cost = MoneyFrame.FormatMoneyString(EA_Window_InteractionHealer.costToRemoveSinglePenalty * EA_Window_InteractionHealer.penaltyCount)
	EA_ChatWindow.Print(towstring(L"You have cleansed yourself of "..EA_Window_InteractionHealer.penaltyCount.. L" penalties" ))
	EA_ChatWindow.Print(towstring(L"This cost you "..cost ))
	EA_Window_InteractionHealer.HealAllPenalties()
	end
